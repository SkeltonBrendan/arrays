
// Arrays
// Brendan Skelton

#include <iostream>
#include <conio.h>
#include <string>
#include <vector>


using namespace std;

struct Student
{
	string name;
	float gpa;
};

int main()
{
	/*
	//On th stack
	const int NUM_STUDENTS
	Student students[NUM_STUDENTS];

	for (int i = 0; i < NUM_STUDENTS; i++) 
	{
		cout << "Name of student " << (i + 1) << ": ";
		cin >> students[i].name;

		cout << "GPA of student " << (i + 1) << ": ";
		cin >> students[i].gpa;
	}
	*/

	/*
	//on the heap
	cout << "How many students? ";
	int numStudents = 0;
	cin >> numStudents;

	Student *pStudents = new Student[numStudents];
	
	for (int i = 0; i < numStudents; i++)
	{
		cout << "Name of student " << (i + 1) << ": ";
		cin >> pStudents[i].name;
		cout << "GPA of student " << (i + 1) << ": ";
		cin >> pStudents[i].gpa;
	}
	*/

	vector<Student> students;
	char another = 'y';

	while (another == 'y' || another == 'Y')
	{
		Student s;
		cin >> s.name;
		cin >> s.gpa;
		students.push_back(s); // Like list.Add(student) in C#

		cout << "Another?";
		cin >> another;
	}

	// slow way to iterate through vector
	/*
	for (int i = 0; i < students.size(); i++)
	{
		cout << students[i].name << ": " << students[i].gpa << "\n";
	}
	*/

	float sum = 0;

	// faster (but ugly)
	vector<Student>::iterator it = students.begin();
	for (; it != students.end(); it++)
	{
		cout << it->name << ": " << it->gpa << "\n";
		sum += it->gpa;
	}

	float avg = sum / students.size();
	cout << "The class avg: " << avg;

	_getch();
	return 0;
}
